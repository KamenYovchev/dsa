
class Node:

	def __init__(self, data):
		self.data = data
		self.next_node = None

	def __repr__(self):
		return str(self.data)
		

class LinkedList:

	def __init__(self):
		# This is the first node of the list
		# WE CAN ACCESS THIS NODE EXCLUSIVELY !!!
		self.head = None
		self.num_of_nodes = 0
	
	def insert_start(self, data):
		self.num_of_nodes += 1
		new_node = Node(data)

		
		# the head is NULL (so the data structure is empty)
		if self.head is None:
			self.head = new_node
		# so this is when linked list is not empty
		else:
			# we have to update the reference
			new_node.next_node = self.head
			self.head = new_node
	
	#O(N)
	def insert_end(self, data):
		self.num_of_nodes += 1
		new_node = Node(data)

		
		# check lined_list is empty
		if self.head is None:
			self.head = new_node
		else:
			# This is when the linked_list is not empty
			actual_node = self.head
			
			# this is why ot has O(N) linear running tie complexity
			while actual_node.next_node is not None:
				actual_node = actual_node.next_node
			
			# actual_node is the last node: so we insert the new_node
			# right after the actual_node
			actual_node.next_node = new_node
	# O(1) constant running time

	#O(1) constant running time
	def size_of_list(self):
		return self.num_of_nodes

	# O(N) linear running time
	def travese(self):

		actual_node = self.head
		i = 1
		while actual_node is not None:
			print(f'{actual_node} is {i}')
			print(f"{actual_node.next_node} is next node ")
			i += 1
			actual_node = actual_node.next_node

	# O(N) Linear time complexity
	def remove(self, data):
		# the list is empty
		if self.head is None:
			return

		actual_node = self.head
		previous_node  = None

		while actual_node is not None and actual_node.data != data:
			previous_node = actual_node
			actual_node =  actual_node.next_node

		if actual_node is None:
			return

		if previous_node is None:
			self.head = actual_node.next_node
		else:
			previous_node.next_node =actual_node.next_node


if __name__ == '__main__':
	linked_list = LinkedList()
	linked_list.insert_start(10)
	linked_list.insert_start('Kamen')
	linked_list.insert_start(8)
	linked_list.insert_start('Dilyana')
	linked_list.insert_end(100)
	linked_list.insert_end(1000)
	linked_list.travese()
	print('--------')
	linked_list.remove(1000)
	linked_list.travese()
	
	

