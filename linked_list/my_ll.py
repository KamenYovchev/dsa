class Node:
	def __int__(self, data):
		self.data = data
		self.next_node = None
		
class LinkedList:
	def __int__(self):
		self.head = None
		self.num_of_nodes = 0
		
	
	def insert_start(self, data):
		'''
		insert node from the beginning
		:param data: data will node reference to
		:return: ceated node
		'''
		
		new_node = Node(data)
		self.num_of_nodes += 1
		
		
		if self.head == None:
			self.head = new_node
		else:
			new_node.next_node = self.head
			self.head = new_node
			
	def insert_end(self, data):
		new_node = Node(data)
		new_node.next_node = None
		self.num_of_nodes += 1
		
		actual_node = self.head
		
		
		while actual_node.next_node is not None:
			actual_node = actual_node.next_node
			
		actual_node.next_node = new_node
		
		