


def calculate_rec_tail(n): #  Tail Recursion
	if n > 0:
		k = n ** 2
		print(k)
		calculate_rec_tail(n - 1)

def calculate_rec_head(n): # Head Recursion
	if n > 0:
		calculate_rec_head(n - 1)
		k = n ** 2
		print(k)


def calculate_rec_tree(n):  # Recursion
	if n > 0:
		calculate_rec_tree(n - 1)
		k = n ** 2
		print(k)
		calculate_rec_tree(n - 1)


# print(calculate_rec_tail(4))
# print(calculate_rec_head(4))
print(calculate_rec_tree(3))


def calculate_itr(n): # Iteration
	while n > 0:
		k = n ** 2
		print(k)
		n -= 1
# calculate_itr(4)

def factorial_rec(n):
	if n == 0:
		return 1
	return factorial_rec(n - 1) * n
	
print(factorial_rec(4))


def num_decreasing(n):
	print(n)
	if n <= 0:
		return 1
	else:
		return n * num_decreasing(n - 1)


num_decreasing(10)


def greet(name):
	print("hello " + name + "!")
	greet2(name)
	print("getting ready to say bye")
	bye()


def greet2(name):
	print("how are you, " + name + "?")


def bye():
	print("ok bye!")


greet("Kamen")


def recur_fib(n):
	if n <= 1:
		return n
	else:
		print(recur_fib(n - 1) + recur_fib(n - 2))
		return (recur_fib(n - 1) + recur_fib(n - 2))


nterms = 10

if nterms <= 0:
	print("Please enter a positive integer")
else:
	print("Fibonacci sequence:")
	for i in range(nterms):
		print(recur_fib(i))
		
		


	
