#
# array = [10, 3, 7, 5]
#
# print(array[::-1])

def reverse(nums):
	'''
	 Function to reverse item in array
	'''
	
	start_index = 0  # Pointing to the first item in the array
	last_index = len(nums) - 1 # pointing to the last item in the array
	
	while start_index < last_index:
		nums[start_index], nums[last_index] = nums[last_index], nums[start_index]
		start_index += 1
		last_index -= 1
	

if __name__ == '__main__':
	array_example = [1, 2, 3, 4, 5]
	print(array_example)
	reverse(array_example)
	print(array_example)
	