def reversed_int(num):
	reversed = 0
	reminder = 0
	
	while (num > 0):
		reminder = num % 10
		num = num // 10
		reversed = reversed * 10 + reminder
		
	return reversed

print(reversed_int(1234))