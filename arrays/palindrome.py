def is_palindrom(word):
	original_string = word
	reversed_string = reverse(word)
	
	if original_string == reversed_string:
		return True
	
	return False


def reverse(word):
	start_index = 0
	end_index = len(word) - 1
	while start_index < end_index:
		word = list(word)
		word[start_index], word[end_index] = word[end_index], word[start_index]
		start_index += 1
		end_index -= 1
	print(word)
	return ''.join(word)





# def palindrome(word):
# 	if word == word[::-1]:
# 		return True
# 	return False


if __name__ == '__main__':
	print(is_palindrom('car'))
